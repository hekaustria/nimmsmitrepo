using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Java.Util;

namespace OSMDroid1
{
    class DownloadListAdapter : BaseAdapter
    {
        private Context context;

        private List<string> list;

        private int selectedItem=-1;

        public DownloadListAdapter(Context context)
        {
            this.context = context;
            list = new List<string>();
        }

        public override int Count
        {
            get
            {
                return list.Count();
            }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return list.ElementAt(position);
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            TextView mapText;
            Button downloadButton;
            
            if (convertView == null)
            {
                convertView = LayoutInflater.From(context).Inflate(Resource.Layout.DownloadListItem, parent, false);
                mapText = (TextView)convertView.FindViewById(Resource.Id.downloadmaptext);
                downloadButton = (Button)convertView.FindViewById(Resource.Id.downloadmapbutton);
                downloadButton.FocusableInTouchMode = false;
                downloadButton.Focusable = false;
                
                convertView.Tag = new ViewHolder(mapText, downloadButton);
            } else
            {
                ViewHolder viewHolder = (ViewHolder) convertView.Tag;
                mapText = viewHolder.mapText;
                downloadButton = viewHolder.downloadButton;
            }
            mapText.SetTextColor(Android.Graphics.Color.White);
            if(position == selectedItem)
            {
                convertView.SetBackgroundColor(Android.Graphics.Color.DarkGreen);
                
            } else
            {
                convertView.SetBackgroundColor(Android.Graphics.Color.Black);
            }
            mapText.Text = GetItem(position).ToString();
            downloadButton.SetOnClickListener(new OnDownloadButtonClickedListener(mapText.Text, context));
            return convertView;
        }

        private class ViewHolder : Java.Lang.Object
        {
            public TextView mapText;
            public Button downloadButton;

            public ViewHolder(TextView t, Button b)
            {
                mapText = t;
                downloadButton = b;
            }
        }

        public void updateList(List<string> list)
        {
            this.list = list;
            base.NotifyDataSetChanged();
        }

        public void updateList()
        {
            base.NotifyDataSetChanged();
        }

        public void setSelectedItem(int selected)
        {
            this.selectedItem = selected;
            updateList();
        }

        public int getSelectedItem()
        {
            return this.selectedItem;
        }

        public Context getContext()
        {
            return context;
        }
    }
}