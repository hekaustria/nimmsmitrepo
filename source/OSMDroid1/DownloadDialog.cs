using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OSMDroid1
{
    class DownloadDialog
    {
        public static int idx = 1;
        public void CreateDialog(Context context, String mapType)
        {
            MapService mapService = AndroidMapService.getInstance();
            LayoutInflater layoutInflater = LayoutInflater.From(context);
            View downloadView = layoutInflater.Inflate(Resource.Layout.DownloadDialogLayout, null);
            var builder = new AlertDialog.Builder(context);
            builder.SetTitle("Download Map");
            

            builder.SetView(downloadView);

            EditText editTextmapName = (EditText)downloadView.FindViewById(Resource.Id.mapname);
            if (!mapService.existsMap(mapType))
            {
                editTextmapName.Text = mapType;
            }
            else
            {
                editTextmapName.Text = mapType + idx;
                idx++;
            }



            builder.SetPositiveButton("Download", (EventHandler<DialogClickEventArgs>)null);
            builder.SetNegativeButton("Cancel", (EventHandler<DialogClickEventArgs>)null);
            var dialog = builder.Create();




            dialog.Show();

            Spinner spinner = (Spinner)dialog.FindViewById(Resource.Id.zoom_level_spinner);
            //ArrayAdapter adapter = ArrayAdapter.CreateFromResource(context, Resource.Array.zoom_levels, Android.Resource.Layout.SimpleSpinnerItem);
            ArrayAdapter adapter = new ArrayAdapter(context, Android.Resource.Layout.SimpleSpinnerItem, AndroidMapService.getInstance().getPossibleZoomValues(mapType));
            adapter.SetDropDownViewResource(Android.Resource.Layout.SimpleSpinnerDropDownItem);
            spinner.Adapter = adapter;

            var downBtn = dialog.GetButton((int)DialogButtonType.Positive);
            var cancelBtn = dialog.GetButton((int)DialogButtonType.Negative);

            downBtn.Click += (sender, args) =>
            {
                EditText mapName = (EditText)dialog.FindViewById(Resource.Id.mapname);
                String stringMapName = mapName.Text;

                if (!mapService.existsMap(stringMapName))
                {
                    
                    mapService.downloadMap(stringMapName, mapType, int.Parse(spinner.SelectedItem.ToString()));
                    dialog.Dismiss();
                }
                else
                {
                    Toast.MakeText(context, "Map name already exists", ToastLength.Short).Show();
                }

                
            };

            cancelBtn.Click += (sender, args) =>
            {
                dialog.Dismiss();
            };

        }
    }
}