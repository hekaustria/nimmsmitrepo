using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OSMDroid1
{
    class CurrentUserDialog
    {
        public void CreateDialog(Context context, String tenantid, String username)
        {
            MapService mapService = AndroidMapService.getInstance();
            
            LayoutInflater layoutInflater = LayoutInflater.From(context);
            View currentUserView = layoutInflater.Inflate(Resource.Layout.CurrentUserDialogLayout,null);
            var builder = new AlertDialog.Builder(context);
            builder.SetTitle("Current User");
            builder.SetView(currentUserView);

            TextView textViewTenantId = (TextView)currentUserView.FindViewById(Resource.Id.dialog_tenantid);
            textViewTenantId.Text = tenantid;

            TextView textViewUsername = (TextView)currentUserView.FindViewById(Resource.Id.dialog_username);
            textViewUsername.Text = username;

            //builder.SetView(Resource.Layout.DownloadDialogLayout);
            //builder.SetMessage("TenantID: " + tenantid);



            builder.SetPositiveButton("Continue", (EventHandler<DialogClickEventArgs>)null);
            builder.SetNegativeButton("Logout", (EventHandler<DialogClickEventArgs>)null);
            var dialog = builder.Create();




            dialog.Show();

            var contBtn = dialog.GetButton((int)DialogButtonType.Positive);
            var logBtn = dialog.GetButton((int)DialogButtonType.Negative);

            contBtn.Click += (sender, args) =>
            {

                dialog.Dismiss();

            };

            logBtn.Click += (sender, args) =>
            {
                mapService.logout();
                Intent intent = new Intent(context, typeof(LoginActivity));
                context.StartActivity(intent);
                dialog.Dismiss();
            };

        }
    }
}