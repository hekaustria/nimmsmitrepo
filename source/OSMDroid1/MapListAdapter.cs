using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Java.Util;

namespace OSMDroid1
{
    class MapListAdapter : BaseAdapter
    {
        private Context context;

        private List<ProgressFile> mapList;

        private int selectedItem;

        private Handler handler;

        public MapListAdapter(Context context)
        {
            this.context = context;
        }

        public override int Count
        {
            get
            {
                return mapList.Count();
            }
        }

        public override Java.Lang.Object GetItem(int position)
        {
            return mapList.ElementAt(position);
            
        }

        public override long GetItemId(int position)
        {
            return position;
        }

        
        /*
        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            TextView mapText;
            Button viewButton;
            Button deleteButton;
            ProgressBar progBar;

            if (convertView == null)
            {
                convertView = LayoutInflater.From(context).Inflate(Resource.Layout.DownloadedMapListItemFinished, parent, false);
                mapText = (TextView)convertView.FindViewById(Resource.Id.maptext);
                viewButton = (Button)convertView.FindViewById(Resource.Id.viewmapbutton);
                deleteButton = (Button)convertView.FindViewById(Resource.Id.deletemapbutton);
                progBar = (ProgressBar)convertView.FindViewById(Resource.Id.progressbar);

                viewButton.Focusable = false;
                viewButton.FocusableInTouchMode = false;

                deleteButton.FocusableInTouchMode = false;
                deleteButton.Focusable = false;

                convertView.Tag = new ViewHolder(mapText, viewButton, deleteButton, progBar);
            }
            else
            {
                ViewHolder viewHolder = (ViewHolder)convertView.Tag;
                mapText = viewHolder.mapText;
                viewButton = viewHolder.viewButton;
                deleteButton = viewHolder.deleteButton;
                progBar = viewHolder.progBar;
            }

            mapText.Text = (string)((Java.IO.File)GetItem(position)).Name;
            viewButton.SetOnClickListener(new OnViewMapClickedListener(mapText.Text, context));
            deleteButton.SetOnClickListener(new OnDeleteMapButtonClicked(position, context));
            progBar.Progress = ((ProgressFile)GetItem(position)).getProgress();

            return convertView;
        }
        */

        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            ProgressFile file = (ProgressFile)GetItem(position);
            View v;

            TextView mapText;
            Button viewButton;
            Button deleteButton;
            ImageButton cancelButton;
            ProgressBar progBar;

            if (file.getProgress() >= 100) //download has already been finished
            {
                v = LayoutInflater.From(context).Inflate(Resource.Layout.DownloadedMapListItemFinished, parent, false);
                mapText = (TextView)v.FindViewById(Resource.Id.maptext);
                viewButton = (Button)v.FindViewById(Resource.Id.viewmapbutton);
                deleteButton = (Button)v.FindViewById(Resource.Id.deletemapbutton);
                

                viewButton.Focusable = false;
                viewButton.FocusableInTouchMode = false;

                deleteButton.FocusableInTouchMode = false;
                deleteButton.Focusable = false;

                viewButton.SetOnClickListener(new OnViewMapClickedListener(((ProgressFile)GetItem(position)).Name, context));
                deleteButton.SetOnClickListener(new OnDeleteMapButtonClicked(position, context));
            } else //download in process 
            {
                v = LayoutInflater.From(context).Inflate(Resource.Layout.DownloadedMapListItemInProcess, parent, false);
                mapText = (TextView)v.FindViewById(Resource.Id.maptext_inprocess);
                cancelButton = (ImageButton)v.FindViewById(Resource.Id.cancelbutton);
                progBar = (ProgressBar)v.FindViewById(Resource.Id.progressbar);
                

                cancelButton.Focusable = false;
                cancelButton.FocusableInTouchMode = false;
                cancelButton.SetOnClickListener(new CancelButtonOnClickListener((string)((ProgressFile)GetItem(position)).Name,context));
                

                progBar.Progress = ((ProgressFile)GetItem(position)).getProgress();
            }

            mapText.Text = (string)((ProgressFile)GetItem(position)).Name;
            mapText.Text = mapText.Text.Replace(".mbtiles", "");
            return v;
        }

        private class ViewHolder : Java.Lang.Object
        {
            public TextView mapText;
            public Button viewButton;
            public Button deleteButton;
            public ProgressBar progBar;

            public ViewHolder(TextView t, Button viewB, Button deleteB, ProgressBar progBar)
            {
                mapText = t;
                viewButton = viewB;
                deleteButton = deleteB;
                this.progBar = progBar;
            }
        }

        public void updateList(List<ProgressFile> list)
        {
            this.mapList = list;
            base.NotifyDataSetChanged();
        }

        public void updateList()
        {
            base.NotifyDataSetChanged();
        }

        public void setSelectedItem(int selectedItem)
        {
            this.selectedItem = selectedItem;
            updateList();
        }

        public int getSelectedItem()
        {
            return selectedItem;
        }
    }
}