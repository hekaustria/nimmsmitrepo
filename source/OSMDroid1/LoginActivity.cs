using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Interop;

namespace OSMDroid1
{
    [Activity(Label = "Login")]
    public class LoginActivity : Activity
    {
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            this.SetContentView(Resource.Layout.login);
        }

        [Export("login")]
        public void login(View v)
        {

            EditText tenantIdText = (EditText)FindViewById(Resource.Id.tenantid);
            EditText usernameText = (EditText)FindViewById(Resource.Id.username);
            EditText passwordText = (EditText)FindViewById(Resource.Id.password);

            string tenantId = tenantIdText.Text;
            string username = usernameText.Text;
            string password = passwordText.Text;

            AndroidMapService service = AndroidMapService.getInstance();

            try
            {
                service.loadToken(tenantId, username, password);
                service.loadMapsInGeoCloud();
                StartActivity(new Intent(this, typeof(MainActivity)));
            }catch(InvalidLoginException e)
            {
                OurErrorDialog ourErrorDialog = new OurErrorDialog();
                ourErrorDialog.CreateDialog("Error", "TenantId, Username or Password invalid!", v.Context);
            }
            
        }
    }
}