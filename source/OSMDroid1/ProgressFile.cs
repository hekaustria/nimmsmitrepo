using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OSMDroid1
{
    class ProgressFile : Java.IO.File
    {
        private int progress;

        public ProgressFile(String path) : base(path)
        {

        }

        public int getProgress()
        {
            return progress;
        }

        public void setProgress(int prog)
        {
            this.progress = prog;
        }

        //Converts Java.IO.File-Array to ProgressFile-Array and sets progress to 100
        public static ProgressFile[] toProgressFiles(Java.IO.File[] files)
        {
            ProgressFile[] progFiles = new ProgressFile[files.Length];

            for(int idx = 0; idx < files.Length; idx++)
            {
                progFiles[idx] = toProgressFile(files[idx]);
            }

            return progFiles;
        }

        public static ProgressFile toProgressFile(Java.IO.File f)
        {
            ProgressFile file = new ProgressFile(f.Path);
            file.setProgress(100);

            return file;
        }

        public override bool Delete()
        {
            return base.Delete();
        }

    }
}