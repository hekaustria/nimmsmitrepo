using System;
using System.Text;
using System.Net;
using System.IO;
using Java.Util;
using Android.Content;
using System.Collections.Generic;
using Java.IO;
using Android.Content.Res;
using BruTile;
using System.ComponentModel;
using System.Linq;
using System.Xml;

using System.Threading.Tasks;


using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace OSMDroid1
{
    abstract class MapService
    {

        protected string username;

        protected string pw;

        protected string tenantId;

        protected List<string> geoCloudMaps = new List<string>();

        public const string FILENAME = "GeoCloudMaps.txt";

        protected string token;

        private List<ProgressFile> downloadedMapFiles;

        private Dictionary<string, MapDownloadBackgroundWorker> backgroundWorkers = new Dictionary<string, MapDownloadBackgroundWorker>();

        private List<IDownloadListener> downloadListener = new List<IDownloadListener>();

        public static Java.IO.File MAP_ROOT_DIR = new Java.IO.File(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal) + "/mbtiles");

        public abstract string getUserName();
        public abstract string getPw();
        public abstract string getTenantId();
        public abstract string getToken();

        public abstract void storeUser();
        public abstract void writeGeoCloudMapsToFile();
        public abstract void readGeoCloudMaps();
        protected abstract double calculateResolution(BruTile.Extent extent);
        public abstract string getCapabilitiesFromXMLFile();
        public abstract void logout();



        //login
        public void loadToken(string tenantId, string name, string pw)
        {
            try
            {

                //Real code! 

                tenantId = tenantId.Trim();
                name = name.Trim();
                pw = pw.Trim();
                pw = pwGeneration(pw);

                //Account Lukas (test) 

                tenantId = "htladmin";
                name = "lukas.eberhardt@htlpinkafeld.at";
                pw = "passwort";
                pw = pwGeneration(pw);


                //encode invalid characters (+, =, ...); % has to be encoded first!  
                pw = pw.Replace("%", "%25");
                pw = pw.Replace("+", "%2B");
                pw = pw.Replace("=", "%3D");
                pw = pw.Replace(" ", "%20");
                pw = pw.Replace("!", "%21");
                pw = pw.Replace("\"", "%22");
                pw = pw.Replace("#", "%23");
                pw = pw.Replace("$", "%24");
                pw = pw.Replace("&", "%26");
                pw = pw.Replace("'", "%27");
                pw = pw.Replace("(", "%28");
                pw = pw.Replace(")", "%29");
                pw = pw.Replace("*", "%2A");
                pw = pw.Replace(",", "%2C");
                pw = pw.Replace("/", "%2F");
                pw = pw.Replace(":", "3A");
                pw = pw.Replace(";", "3B");
                pw = pw.Replace("?", "%3F");
                pw = pw.Replace("@", "%40");
                pw = pw.Replace("[", "%5B");
                pw = pw.Replace("\\", "%5C");
                pw = pw.Replace("]", "%5D");
                pw = pw.Replace("{", "%7B");
                pw = pw.Replace("|", "%7C");
                pw = pw.Replace("}", "7D");




                //Account Andreas (test)

                name = "jusits@rmdata-geospatial.com";
                pw = "0bVu556HigsAuzommzJGDaHnNK0gJkV5pogrgcNg8Oaf5NZBlD%2FAiXL61Hlr8gyT5swIVgk4QY3%2FApsJVA3WQQ%3D%3D";
                tenantId = "htladmin";


                HttpWebRequest loginRequest = null;
                HttpWebResponse loginResponse = null;
                string loginUrl = "http://htladmin.rmdatacloud.com:8080/token";
                WebHeaderCollection loginHeader = new WebHeaderCollection();

                loginHeader.Set("TenantId", tenantId);

                loginRequest = (HttpWebRequest)WebRequest.Create(loginUrl);
                loginRequest.Method = WebRequestMethods.Http.Post;
                loginRequest.Headers = loginHeader;
                loginRequest.ContentType = "application/x-www-form-urlencoded";
                Stream loginStream = loginRequest.GetRequestStream();
                byte[] body = Encoding.UTF8.GetBytes("grant_type=password&username=" + name + "&password=" + pw);
                loginStream.Flush();
                loginStream.Write(body, 0, body.Length);

                loginResponse = (HttpWebResponse)loginRequest.GetResponse();

                if (loginResponse != null)
                {
                    this.username = name;
                    this.pw = pw;
                    this.tenantId = tenantId;

                    Stream loginResponseStream = loginResponse.GetResponseStream();
                    int bufferSize = 1024;
                    byte[] readBytes = new byte[bufferSize];
                    loginResponseStream.Read(readBytes, 0, bufferSize);
                    string authorization = Encoding.UTF8.GetString(readBytes);
                    authorization = authorization.Remove(0, 17);
                    string[] bearer = authorization.Split("\"".ToCharArray());
                    authorization = "Bearer " + bearer[0];
                    this.token = authorization;

                    storeUser();
                }

            }
            catch (Exception e)
            {
                throw new InvalidLoginException();
            }
        }



        public void setDownloadListener(IDownloadListener listener)
        {
            this.downloadListener.Add(listener);
        }

        public List<IDownloadListener> getDownloadListener()
        {
            return this.downloadListener;
        }

        public List<string> getMapsInGeoCloud()
        {
            return geoCloudMaps;
        }

        public static void SetAuthorizationToken(HttpWebRequest client, string token, string tenant)
        {
                client.Headers.Add("tenantid", tenant);
            //client.Credentials = null;

          
                client.Headers.Add("Authorization", "Bearer " + token);//HttpContext.Current.Session["bearer"].ToString());
        }

        // GET REQUEST: GetMaps
        public static List<OSMDroid1.Model.Map> GetMaps(string token)
        {
            WebHeaderCollection headers = new WebHeaderCollection();
            string uri = "http://htladmin.rmdatacloud.com:8080/api/maps";


            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            {
                SetAuthorizationToken(request, token, "htladmin");
             
                using (WebResponse response = request.GetResponse())
                {
                    if (response != null)
                    {
                        var rawJson = new StreamReader(response.GetResponseStream()).ReadToEnd();

                        return JsonConvert.DeserializeObject<List<OSMDroid1.Model.Map>>(rawJson);
                        //Newtonsoft.Json.JsonSerializer ser = new JsonSerializer();
                        //return ser.Deserialize<List<rmdata.Cloud.Common.Model.Map>>(rawJson);
                        //var formatter = new JsonMediaTypeFormatter
                        //{
                        //    SerializerSettings = { TypeNameHandling = TypeNameHandling.Auto }
                        //};
                        //return await .ReadAsAsync<List<rmdata.Cloud.Common.Model.Map>>(new List<MediaTypeFormatter> { formatter });
                    }
                    else
                        return new List<OSMDroid1.Model.Map>();
                }
            }
        }

        //downloads all GeoCloud maps 
        public void loadMapsInGeoCloud()
        {
            bool reloaded = false;

            try
            {


                HttpWebRequest request = null;
                HttpWebResponse response = null;
                WebHeaderCollection headers = new WebHeaderCollection();
                string url = "http://htladmin.rmdatacloud.com:8080/api/maps";

                ////Load maps
                //if (token != null)
                //{
                //    try
                //    {

                //        headers.Set("Authorization", token);

                //        request = (HttpWebRequest)WebRequest.Create(url);
                //        request.Headers = headers;
                //        request.Method = WebRequestMethods.Http.Get;
                //        request.Headers = headers;
                //        response = (HttpWebResponse)request.GetResponse();
                //    }
                //    catch (Exception ex)
                //    {
                //        throw new NoGeoCloudAccessException();
                //    }
                //}
                //if (response != null)
                //{
                //    this.geoCloudMaps = new List<string>();
                //    Stream responseStream = response.GetResponseStream();
                //    Scanner scanner = new Scanner(responseStream);
                //    if (scanner.HasNextLine)
                //    {
                //        string mapname = scanner.NextLine();
                //        mapname = mapname.Replace("[", "");
                //        mapname = mapname.Replace("\"", "");
                //        mapname = mapname.Replace("]", "");
                //        mapname = mapname.Trim();
                //        string[] maps = mapname.Split(',');
                //        foreach (string s in maps)
                //        {
                //            if (!s.Equals(""))
                //            {
                //                geoCloudMaps.Add(s);

                //            }
                //        }
                //    }
                //    writeGeoCloudMapsToFile();
                //}
                var listofmaps = GetMaps(token);
                List<string> mapnames = new List<string>();

                foreach( var map in listofmaps)
                {
                    mapnames.Add(map.Name);
                    //geoCloudMaps.Add(map.Name);
                }
                geoCloudMaps = mapnames;
                writeGeoCloudMapsToFile();
            }
            catch (Exception ex)
            {
                //reload token - if expired
                //only try one reload (therefore the reload flag!) 
                if (!reloaded)
                {
                    loadToken(getTenantId(), getUserName(), getPw());
                    reloaded = true;
                    loadMapsInGeoCloud();
                }
            }

        }

        private static string pwGeneration(string value)
        {
            using (var sha512 = System.Security.Cryptography.SHA512Managed.Create())
            {

                return Convert.ToBase64String(sha512.ComputeHash(Encoding.UTF8.GetBytes(value)));
            }

        }

        public bool existsMap(string mapname)
        {
            foreach (ProgressFile map in downloadedMapFiles)
            {
                if (map.Name.Equals(mapname + ".mbtiles"))
                {
                    return true;
                }
            }
            return false;
        }

        public void downloadMap(string mapName, string mapType, int zoomLevel)
        {
            downloadButtonClicked();  
            MapDownloadBackgroundWorker worker = new MapDownloadBackgroundWorker(mapName, mapType, zoomLevel);
            worker.DoWork += download;
            worker.ProgressChanged += fireProgressChanged;
            worker.RunWorkerCompleted += downloadComplete;
            worker.WorkerSupportsCancellation = true;
            worker.WorkerReportsProgress = true;

            backgroundWorkers.Add(mapName, worker);
            worker.RunWorkerAsync();
            
            
        }

        public void downloadButtonClicked()
        {
            foreach (var dL in downloadListener)
            {
                dL.downloadButtonClicked();
            }
                        
        }

        public void download(object sender, DoWorkEventArgs e)
        {


            MapDownloadBackgroundWorker worker = sender as MapDownloadBackgroundWorker;
            string mapType = worker.getMapType();
            string mapName = worker.getMapName();
            int zoomLevel = worker.getZoomLevel();

            if (!MAP_ROOT_DIR.IsDirectory)
            {
                MAP_ROOT_DIR.Mkdir();
            }

            ProgressFile map = new ProgressFile(MAP_ROOT_DIR.Path + "/" + mapName + ".mbtiles");

            try
            {


                string capabilities = getCapabilitiesByRequest(mapType);

                addDownloadedMap(map);
                downloadStarted();

                Database db = new Database(map.Path);

                using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(capabilities)))
                {
                    var tileSources = BruTile.Wmts.WmtsParser.Parse(stream);
                    
                    //Get tiles 
                    int tileCount = 0;
                    List<Tile> tileList = new List<Tile>();
                    int i = 0;
                    foreach (var tileSource in tileSources)
                    {

                        
                        if (tileSource.Schema.Name.Contains("3857"))
                        {

                            
                            BruTile.Extent extent = getExtent(mapType);

                            double resolution = calculateResolution(extent);
                            double minX = extent.MinX;
                            double minY = extent.MinY;
                            double maxX = extent.MaxX;
                            double maxY = extent.MaxY;


                            //Add metadata "bounds" and "username" to database 
                            List<Metadata> metadata = new List<Metadata>();
                            string bounds = Convert3857ToWGS84(minX, minY) + ", " + Convert3857ToWGS84(maxX, maxY);
                            metadata.Add(new Metadata("bounds", bounds));
                            metadata.Add(new Metadata("username", getUserName()));

                            
                            var minZoom = BruTile.Utilities.GetNearestLevel(tileSource.Schema.Resolutions, resolution);
                            
                            var maxZoom = zoomLevel; //assign real maxzoom
                            metadata.Add(new Metadata("max_zoom", maxZoom.ToString()));
                            metadata.Add(new Metadata("min_zoom", minZoom)); 

                            db.AddMetadataToDatabase(metadata);

                            tileCount = getTileCount(tileSources, extent, int.Parse(minZoom), maxZoom);



                            for (int start = int.Parse(minZoom); start <= maxZoom; start++)
                            {
                                var tiles = tileSource.Schema.GetTilesInView(extent, start.ToString());
                                try
                                {
                                    foreach (var tile in tiles)
                                    {
                                        if (!worker.getCancel())
                                        {
                                            Extent tileExtent = tile.Extent;
                                            double tileMinX = tileExtent.MinX;
                                            double tileMinY = tileExtent.MinY;
                                            double tileMaxX = tileExtent.MaxX;
                                            double tileMaxY = tileExtent.MaxY;

                                            int tmsRow = (1 << zoomLevel) - 1 - tile.Index.Row;

                                            string imageUrl = downloadImage(mapType, tile.Index.Row, tile.Index.Col, tile.Index.Level, tileSource.Schema.Name); //tileSource.Schema.Name = coordinate reference system 
                                            tileList.Add(new Tile(System.Convert.ToInt32(tile.Index.Level), System.Convert.ToInt32(tile.Index.Col), System.Convert.ToInt32(tmsRow), imageUrl));
                                            i++;

                                            int prog = (i * 100 / tileCount);
                                            map.setProgress(prog);
                                            worker.ReportProgress(prog);
                                        }
                                        else
                                        {
                                            map.Delete();
                                            backgroundWorkers.Remove(mapName);
                                            downloadedMapFiles.Remove(map);
                                            return;

                                        }


                                    }
                                }
                                catch (KeyNotFoundException knfEx)
                                {
                                    //! 
                                }

                            }

                        }
                    }

                    //add tiles to database 
                    db.AddTilesToDatabase(tileList);
                    backgroundWorkers.Remove(mapName);
                }
            }
            catch (Exception ex)
            {
                this.backgroundWorkers.Remove(mapName);
                this.downloadedMapFiles.Remove(map);
                if (map.Exists())
                {
                    map.Delete();
                }
            }
        }

        public List<string> getPossibleZoomValues(string mapType)
        {
            string capabilities = getCapabilitiesByRequest(mapType);
            List<string> levels = new List<string>();
            using (MemoryStream stream = new MemoryStream(Encoding.UTF8.GetBytes(capabilities)))
            {
                var tileSources = BruTile.Wmts.WmtsParser.Parse(stream);

                foreach (var tileSource in tileSources)
                { 

                    if (tileSource.Schema.Name.Contains("3857"))
                    {
                        BruTile.Extent extent = getExtent(mapType);

                        double resolution = calculateResolution(extent);

                        var minZoom = BruTile.Utilities.GetNearestLevel(tileSource.Schema.Resolutions, resolution);

                        var maxZoom = tileSource.Schema.Resolutions.Count;

                        for(int idx = int.Parse(minZoom); idx < maxZoom; idx++){
                            levels.Add(idx.ToString());
                        }
                    }
                }
            }
            return levels; 
        }

        public string getCapabilitiesByRequest(string mapType)
        {
            string cap = "";
            string url = "http://htladmin.rmdatacloud.com:8080/api/maps/" + mapType + "/wmts?SERVICE=WMTS&REQUEST=GETCAPABILITIES";
            HttpWebRequest request;
            HttpWebResponse response;

            request = (HttpWebRequest)HttpWebRequest.Create(url);
            response = (HttpWebResponse)request.GetResponse();
            if (response != null)
            {
                var stream = response.GetResponseStream();
                using (var reader = new StreamReader(stream))
                {
                    cap = reader.ReadToEnd();
                }
            }

            return cap;
        }

        //Get total number of tiles for progress calculation
        //Improve!!!!!!!!!!
        private int getTileCount(IEnumerable<ITileSource> tileSources, BruTile.Extent extent, int startZoom, int endZoom)
        {
            int tileCount = 0;

            foreach (var tileSource in tileSources)
            {
                if (tileSource.Schema.Name.Contains("3857"))
                {
                    for (int z = startZoom; z < endZoom; z++)
                    {
                        var tiles = tileSource.Schema.GetTilesInView(extent, z.ToString());
                        try
                        {
                            foreach (var tile in tiles)
                            {
                                tileCount++;
                            }
                        }
                        catch (KeyNotFoundException ex)
                        {
                            //!
                        }
                    }
                }
            }
            return tileCount;
        }


        public void fireProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            foreach (var dL in downloadListener)
            {
                dL.progressChanged();
            }

        }

        public void downloadComplete(object sender, RunWorkerCompletedEventArgs e)
        {
            foreach (var dL in downloadListener)
            {
                dL.downloadComplete();
            }
        }

        public void downloadStarted()
        {
            foreach (var dL in downloadListener)
            {
                dL.downloadStarted();
            }
        }

        protected double CalculateResolution(BruTile.Extent area, int width, int height, double dpi)
        {
            double worldToPixelX;
            double worldToPixelY;

            if (dpi > 0.0)
            {
                worldToPixelX = area.Width / (width / dpi);
                worldToPixelY = area.Height / (height / dpi);
            }

            else
            {
                worldToPixelX = area.Width / width;
                worldToPixelY = area.Height / height;
            }

            return Math.Min(worldToPixelX, worldToPixelY);
        }



        private BruTile.Extent getExtent(string mapType)
        {
            BruTile.Extent extent = new BruTile.Extent();
            string extentUrl = "http://htladmin.rmdatacloud.com:8080/api/maps/Extents?coordinatereferencesystem=3857&mapname="+mapType; 
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(extentUrl);
            WebResponse response;
            WebHeaderCollection extentHeaders = new WebHeaderCollection();
            extentHeaders.Set("TenantId", getTenantId());
            extentHeaders.Set("Authorization", getToken());
            request.Headers = extentHeaders;
            response = (HttpWebResponse)request.GetResponse();
            if (response != null)
            {
                var raqJson = new StreamReader(response.GetResponseStream()).ReadToEnd();

                JObject jobj =  (JObject) JsonConvert.DeserializeObject(raqJson);

                extent = new BruTile.Extent(jobj["Left"].Value<double>(), jobj["Bottom"].Value<double>(), jobj["Right"].Value<double>(), jobj["Top"].Value<double>());
                //Scanner sc = new Scanner(responseStream);
                //if (sc.HasNextLine)
                //{
                //    string extentText = sc.NextLine();
                //    string[] coordText;
                //    double[] coordinates = new double[4];
                //    extentText = extentText.Replace("\"", "");
                //    extentText = extentText.Replace("top", "");
                //    extentText = extentText.Replace("bottom", "");
                //    extentText = extentText.Replace("left", "");
                //    extentText = extentText.Replace("right", "");
                //    extentText = extentText.Replace(":", "");
                //    extentText = extentText.Replace("{", "");
                //    extentText = extentText.Replace("}", "");


                //    coordText = extentText.Split(',');
                //    coordinates[0] = Convert.ToDouble(coordText[0].Replace(".", ",")); //left
                //    coordinates[1] = Convert.ToDouble(coordText[1].Replace(".", ",")); //bottom
                //    coordinates[2] = Convert.ToDouble(coordText[2].Replace(".", ",")); //right
                //    coordinates[3] = Convert.ToDouble(coordText[3].Replace(".", ",")); //top 
                //    extent = new BruTile.Extent(coordinates[0], coordinates[1], coordinates[2], coordinates[3]);



                //}


            }
            return extent;
        }

        private string downloadImage(String mapName, String mapType, double minX, double minY, double maxX, double maxY, string levelIndex, int row, int col)
        {
            string imageUrl = "";

            string minXString = minX.ToString().Replace(",", ".");
            string minYString = minY.ToString().Replace(",", ".");
            string maxXString = maxX.ToString().Replace(",", ".");
            string maxYString = maxY.ToString().Replace(",", ".");

            string url = " http://htladmin.rmdatacloud.com:8080/api/maps/" + mapType + "/wms?SERVICE=WMS&REQUEST=GetMap&BBOX="
                + minXString + "," + minYString + "," + maxXString + "," + maxYString + "&WIDTH=1090&Height=750&Layers=" + mapType
                + "&FORMAT=image/png&CRS=EPSG:3857&VERSION=1.3.0&Styles=&TRANSPARENT=TRUE";


            HttpWebRequest request = null;
            HttpWebResponse response = null;

            try
            {

                request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = WebRequestMethods.Http.Get; ;
                request.Accept = "image/png";
                request.Host = "htladmin.rmdatacloud.com:8080";
                request.UserAgent = "WMSLayer";
                request.KeepAlive = false;
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception ex)
            {
                throw new DownloadImageException();
            }

            if (response != null)
            {
                String dir = MAP_ROOT_DIR.Path; ;
                if (!MAP_ROOT_DIR.Exists())
                {
                    MAP_ROOT_DIR.Mkdir();

                }
                imageUrl = dir + "/" + levelIndex + " " + row.ToString() + " " + col.ToString() + ".png";

                Stream responseStream = response.GetResponseStream();
                int bufferSize = 1024;
                byte[] readBytes = new byte[bufferSize];
                int count;

                using (var fileStream = System.IO.File.Create(imageUrl))
                {
                    while ((count = responseStream.Read(readBytes, 0, bufferSize)) != 0)
                    {

                        fileStream.Write(readBytes, 0, count);
                        fileStream.Flush();
                    }

                    fileStream.Close();

                }



            }
            return imageUrl;
        }

        private string downloadImage(string mapType, int row, int col, string zLevel, string coordSystem)
        {
            string url = "http://htladmin.rmdatacloud.com:8080/api/maps/" + mapType + "/wmts/" + mapType + "_LAYER/normal/" + coordSystem + "/" + zLevel + "/" + row + "/" + col;
            string imageUrl = "";
            HttpWebRequest request = null;
            HttpWebResponse response = null;

            try
            {

                request = (HttpWebRequest)WebRequest.Create(url);
                request.Method = WebRequestMethods.Http.Get; ;
                request.Accept = "image/png";
                request.Host = "htladmin.rmdatacloud.com:8080";
                request.UserAgent = "WMSLayer";
                request.KeepAlive = false;
                response = (HttpWebResponse)request.GetResponse();
            }
            catch (Exception ex)
            {
                throw new DownloadImageException();
            }

            if (response != null)
            {
                String dir = MAP_ROOT_DIR.Path; ;
                if (!MAP_ROOT_DIR.Exists())
                {
                    MAP_ROOT_DIR.Mkdir();

                }
                imageUrl = dir + "/" + zLevel + " " + row.ToString() + " " + col.ToString() + ".png";

                Stream responseStream = response.GetResponseStream();
                int bufferSize = 1024;
                byte[] readBytes = new byte[bufferSize];
                int count;

                using (var fileStream = System.IO.File.Create(imageUrl))
                {
                    while ((count = responseStream.Read(readBytes, 0, bufferSize)) != 0)
                    {

                        fileStream.Write(readBytes, 0, count);
                        fileStream.Flush();
                    }

                    fileStream.Close();

                }



            }
            return imageUrl;
        }

        //get all maps, which have already been downloaded 

        private void initDownloadedMapList()
        {

            ProgressFile[] files = null;

            downloadedMapFiles = new List<ProgressFile>();


            Java.IO.File mbFolder = MapService.MAP_ROOT_DIR;

            if (!mbFolder.Exists())
            {
                mbFolder.Mkdir();
                files = ProgressFile.toProgressFiles(new Java.IO.File(mbFolder.AbsolutePath).ListFiles());
            }
            else
            {

                files = ProgressFile.toProgressFiles(new Java.IO.File(mbFolder.AbsolutePath).ListFiles());
            }

            if (files != null && files.Length > 0)
            {

                for (int i = 0; i < files.Length; i++)
                {
                    if (files[i].Name.EndsWith(".mbtiles") && files != null)
                    {
                        //TODO only display maps of current user 
                        MetadataDAO dao = new MetadataDAO(MAP_ROOT_DIR.Path + "/" + files[i].Name);
                        Metadata user = dao.getMetadata("username");
                        if (user.getValue().Equals(getUserName()))
                        {
                            downloadedMapFiles.Add(files[i]);
                        }
                    }
                }
            }


        }

        public void addDownloadedMap(ProgressFile map)
        {
            this.downloadedMapFiles.Add(map);
        }

        public List<ProgressFile> getDownloadedMapFiles()
        {
            if (downloadedMapFiles == null)
            {
                initDownloadedMapList();
            }
            return downloadedMapFiles;
        }

        private static string Convert3857ToWGS84(double mercatorX_lon, double mercatorY_lat)
        {
            if (Math.Abs(mercatorX_lon) < 180 && Math.Abs(mercatorY_lat) < 90)
            {
                return null;
            }

            if ((Math.Abs(mercatorX_lon) > 20037508.3427892) || (Math.Abs(mercatorY_lat) > 20037508.3427892))
            {
                return null;
            }

            double x = mercatorX_lon;
            double y = mercatorY_lat;
            double num3 = x / 6378137.0;
            double num4 = num3 * 57.295779513082323;
            double num5 = Math.Floor((double)((num4 + 180.0) / 360.0));
            double num6 = num4 - (num5 * 360.0);
            double num7 = 1.5707963267948966 - (2.0 * Math.Atan(Math.Exp((-1.0 * y) / 6378137.0)));
            mercatorX_lon = num6;
            mercatorY_lat = num7 * 57.295779513082323;
            return mercatorX_lon.ToString().Replace(",", ".") + ", " + mercatorY_lat.ToString().Replace(",", ".");
        }

        public void cancel(String filename)
        {
            MapDownloadBackgroundWorker backgroundWorker = backgroundWorkers[filename];
            backgroundWorker.setCancel(true);
        }
    }


}