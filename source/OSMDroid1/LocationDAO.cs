﻿using System;
using System.Collections.Generic;
using System.Data;
using Mono.Data.Sqlite;

/*
 *  Changelog
 *  28.11.2014 - STÖHR: Listunterstützung und Try-Catch-Blöcke hinzugefügt
 */

namespace OSMDroid1
{
    /*
     * DAO enthält folgende Funktionen:
     * getLocationByName(String name);
     * getLocationByID(int id);
     * insertLocation(Location loc);
     * deleteLocationByID(int id);
     * deleteLocationByName(String name);
     * 
     */

    //erstellt von Stefan Stöhr, erweitert von Daniel Polzhofer
    class LocationDAO
    {
        private String input_file;
        private IDbConnection connection;

        public LocationDAO(String path) //erstellt von Stefan Stöhr, erweitert von Daniel Polzhofer
        {

            this.input_file = path;
            connection = new SqliteConnection("Data Source=" + this.input_file);

            
        }

        public List<Location> getLocationByName(String name) //erstellt von Stefan Stöhr, erweitert von Daniel Polzhofer
        {
            List<Location> Loc_List = new List<Location>(); //liefert eine Liste der Ergebnisse zurück
            string[] args = { name };

            using (var connection = new SqliteConnection(input_file))
            {
                connection.OpenAsync();
                SqliteCommand command = connection.CreateCommand();
                command.CommandText = "Select * from locations where name like '%" + name + "%'";
                SqliteCommand testcommand = connection.CreateCommand();
                testcommand.CommandText = "select count(*) FROM tiles";

                if (connection.State == ConnectionState.Open)
                {
                    try
                    {
                        var readerTask = command.ExecuteReaderAsync();
                        var reader = readerTask.Result;
                        var testreaderTask = testcommand.ExecuteReaderAsync();
                        var testreader = testreaderTask.Result;

                        while (reader.Read())
                        {
                            int _id = reader.GetInt32(0);
                            double cord_x = reader.GetDouble(1);
                            double cord_y = reader.GetDouble(2);
                            String _name = reader.GetString(3);
                            Loc_List.Add(new Location(_id, cord_x, cord_y, _name));
                        }
                        reader.Close();
                    }
                    catch (SqliteException e)
                    {
                        //Ausgabe Fehlermeldung
                        Console.WriteLine(e);

                    }
                }
                if (Loc_List.Count == 0)
                {
                    Loc_List = null;
                }
                System.Console.WriteLine("Search request executed for:' " + name + "'");
            }

            return Loc_List;
        }

        public List<Location> getLocationByID(int id) //erstellt von Stefan Stöhr, erweitert von Daniel Polzhofer
        {
            List<Location> Loc_List = new List<Location>(); //liefert eine Liste der Ergebnisse zurück

            using (var connection = new SqliteConnection(input_file))
            {
                SqliteCommand command = connection.CreateCommand();
                command.CommandText = "Select * from locations where id = " + id;

                if (connection.State == ConnectionState.Open)
                {
                    try
                    {
                        var readerTask = command.ExecuteReaderAsync();
                        var reader = readerTask.Result;

                        while (reader.Read())
                        {
                            int _id = reader.GetInt32(0);
                            double cord_x = reader.GetDouble(1);
                            double cord_y = reader.GetDouble(2);
                            String _name = reader.GetString(3);
                            Loc_List.Add(new Location(_id, cord_x, cord_y, _name));
                        }
                        reader.Close();
                    }
                    catch (SqliteException e)
                    {
                        //Ausgabe Fehlermeldung
                        Console.WriteLine(e);
                    }
                }
                if (Loc_List.Count == 0)
                {
                    Loc_List = null;
                }
                System.Console.WriteLine("Search request executed for:' " + id + "'");
            }
                

            return Loc_List;
        }

        public Boolean addLocation(int i, long x, long y, String name)
        {
            Boolean check = true;
            using (var connection = new SqliteConnection(input_file))
            {
                SqliteCommand add_command = connection.CreateCommand();
                add_command.CommandText = "insert into locations values (" + i + "," + x + "," + y + ",'" + name + "')";

                try
                {
                    add_command.ExecuteNonQueryAsync();
                }
                catch (SqliteException e)
                {
                    Console.WriteLine(e);
                }
            }
            

            return check;
        }
    }
}
