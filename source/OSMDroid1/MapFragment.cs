using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using Java.Util;
using System;
using System.Collections.Generic;

namespace OSMDroid1
{

    public class MapFragment : ListFragment, IDownloadListener
    {

        private MapListAdapter adapter;

        private Context context;

        public MapFragment()
        {

        }

        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);


        }

        public override View OnCreateView(LayoutInflater inflater,
        ViewGroup container, Bundle savedInstanceState)
        {

            var view = base.OnCreateView(inflater, container, savedInstanceState);
            ShowFiles();

            return view;
        }

        public void ShowFiles()
        {
            var arrayList = new List<ProgressFile>();

            AndroidMapService service = AndroidMapService.getInstance();
            arrayList = service.getDownloadedMapFiles();
            if (arrayList != null && arrayList.Count > 0)
            {
                adapter = new MapListAdapter(context);
                adapter.updateList(arrayList);
                this.ListAdapter = adapter;
            }
            else
            {
                string[] noFilesArray = new string[1];
                noFilesArray[0] = "No elements available";
                adapter = null;
                var noFilesAdapter = new ArrayAdapter(context, Android.Resource.Layout.SimpleListItem1, noFilesArray);
                this.ListAdapter = noFilesAdapter;

            }


        }

        public override void OnResume()
        {
            base.OnResume();
            ShowFiles();
        }

        public void progressChanged()
        {
            if (this.ListAdapter is MapListAdapter)
            {
                ((MapListAdapter)this.ListAdapter).updateList();
            }
        }

        public void downloadComplete()
        {
            progressChanged();
            ShowFiles();
        }

        public void downloadStarted()
        {
            ShowFiles();
        }

        public override void OnAttach(Activity activity)
        {
            base.OnAttach(activity);
            this.context = activity;
        }

        public void downloadButtonClicked()
        {
        }
    }

}

