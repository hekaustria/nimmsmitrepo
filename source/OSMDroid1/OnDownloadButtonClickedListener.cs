using System;
using Android.Views;
using Android.Content;
using System.Threading;
using Android.App;
using Android.Widget;

namespace OSMDroid1
{
    class OnDownloadButtonClickedListener : Java.Lang.Object, View.IOnClickListener 
    {
        private string mapType;
        private Context context;

        public OnDownloadButtonClickedListener(string mapType, Context context)
        {
            this.mapType = mapType;
            this.context = context;
        }
       
        public void OnClick(View v)
        {

            DownloadDialog dialog = new DownloadDialog();
            dialog.CreateDialog(context, mapType);
            
        }

    }
}