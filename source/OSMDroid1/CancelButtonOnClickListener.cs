using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OSMDroid1
{
    class CancelButtonOnClickListener : Java.Lang.Object, View.IOnClickListener
    {
        private string filename;
        Context context;
        public CancelButtonOnClickListener(string filename, Context context)
        {
            this.filename = filename;
            this.context = context;
        }

        public void OnClick(View v)
        {
            Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this.context);
            Android.App.AlertDialog alertDialog = builder.Create();
            alertDialog.SetTitle("Cancel download");
            alertDialog.SetMessage("Do you really want to cancel downloading the map?");
            alertDialog.SetCancelable(false);
            alertDialog.SetCanceledOnTouchOutside(false);
            alertDialog.SetButton("No", (sender, e) =>
            {

            });

            alertDialog.SetButton2("Yes", (sender, e) =>
            {
                AndroidMapService.getInstance().cancel(filename.Replace(".mbtiles", ""));
            });
            alertDialog.Show();
            
        }
    }
}