using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.ComponentModel;

namespace OSMDroid1
{
    class MapDownloadBackgroundWorker : BackgroundWorker
    {
        private string mapName;
        private string mapType;
        private int zoomLevel;
        private bool cancel;

        public MapDownloadBackgroundWorker(string mapName, string mapType, int zoomLevel)
        {
            this.mapName = mapName;
            this.mapType = mapType;
            this.zoomLevel = zoomLevel;
        }

        public string getMapName()
        {
            return mapName;
        }

        public void setMapName(string mapName)
        {
            this.mapName = mapName;
        }

        public string getMapType()
        {
            return mapType;
        }

        public void setMapType(string mapType)
        {
            this.mapType = mapType;
        }

        public int getZoomLevel()
        {
            return zoomLevel;
        }

        public void setZoomLevel(int zoomLevel)
        {
            this.zoomLevel = zoomLevel;
        }

        public bool getCancel()
        {
            return cancel;
        }

        public void setCancel(bool cancel)
        {
            this.cancel = cancel;
        }
    }
}