
using Android.App;
using Android.OS;
using Android.Views;
using Android.Widget;
using Java.Lang;
using Java.Util;
using System.Collections.Generic;
using System;
using Android.Graphics.Drawables;
using Android.Graphics;
using System.Diagnostics;

namespace OSMDroid1
{

    public class DownloadFragment : ListFragment, IDownloadListener
    {
        private AndroidMapService service = AndroidMapService.getInstance();
        private DownloadListAdapter adapter;
        private Dialog dialog;
        private Activity activity;
        public override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            if (this.ListAdapter == null)
            {
                

                //Load maps and store it in adapter
                //var adapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, service.loadMapsInGeoCloud(token).ToArray());
                adapter = new DownloadListAdapter(this.Activity);
                List<string> mapList = service.getMapsInGeoCloud();
                if (mapList.Count > 0)
                {
                    adapter.updateList(mapList);

                    this.ListAdapter = adapter;
                } else
                {
                    string[] noFilesArray = { "No maps in GeoCloud" };
                    this.ListAdapter = new ArrayAdapter(this.Activity, Android.Resource.Layout.SimpleListItem1, noFilesArray);
                }
                

            }

        }

        public override View OnCreateView(LayoutInflater inflater,
       ViewGroup container, Bundle savedInstanceState)
        {
            
            var view = base.OnCreateView(inflater, container, savedInstanceState);
            return view;
        }

        public override void OnViewCreated(View view, Bundle savedInstanceState)
        {
            base.OnViewCreated(view, savedInstanceState);
            this.ListView.Clickable = true;
            this.ListView.Focusable = true;
            this.ListView.ChoiceMode = ChoiceMode.Single;
            
            
        }

        public override void OnListItemClick(ListView l, View v, int position, long id)
        {
            base.OnListItemClick(l, v, position, id);
            adapter.setSelectedItem(position);
        }

        public void downloadStarted()
        {
            dialog.Dismiss();
        }

        public void progressChanged()
        {
            
        }

        public void downloadComplete()
        {
            
        }

        public void downloadButtonClicked()
        {
            //if (this.Activity != null)
            //{
           
                dialog = new Dialog(this.Activity);
                dialog.RequestWindowFeature((int)WindowFeatures.NoTitle);
                dialog.SetCancelable(false);
                dialog.SetCanceledOnTouchOutside(false);
                Window window = dialog.Window;
                window.SetLayout(WindowManagerLayoutParams.MatchParent, WindowManagerLayoutParams.MatchParent);
                window.SetBackgroundDrawable(new ColorDrawable(Color.Transparent));

                var progress = new ProgressBar(this.Activity);
                progress.LayoutParameters = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WrapContent, ViewGroup.LayoutParams.WrapContent);
                progress.Indeterminate = true;
                dialog.AddContentView(progress, progress.LayoutParameters);

                dialog.Show();
            //}    
        }

        public override void OnAttach(Activity activity)
        {
            base.OnAttach(activity);
            this.activity = activity;
        }

        public override void OnDetach()
        {
            base.OnDetach();
            this.activity = null;
        }

        public override void OnDestroy()
        {
            base.OnDestroy();
            this.activity = null;
        }

    }


}