using System;
using System.IO;



namespace OSMDroid1
{
    class Tile
    {
        private int level;
        private int col;
        private int row;
        private byte[] blob;
        private string blob_s;


        public Tile(int level, int col, int row, string blob_s)
        {
            this.level = level;
            this.col = col;
            this.row = row;
            this.blob_s = blob_s;
        }

        public Tile(int level, int col, int row, Byte[] blob)
        {
            this.level = level;
            this.col = col;
            this.row = row;
            this.blob = blob;
        }

        public Tile()
        {

        }

        

        public int GetLevel()
        {
            return this.level;
        }

        public int GetCol()
        {
            return this.col;
        }

        public int GetRow()
        {
            return this.row;
        }


        public string GetBlob_s()
        {
            return this.blob_s;
        }

        public void setLevel(int level)
        {
            this.level = level;
        }

        public void setRow(int row)
        {
            this.row = row;
        }

        public void setCol(int col)
        {
            this.col = col;
        }

        public void setBlob(string blob)
        {
            this.blob_s = blob;
        }

        public byte[] GetBlob()
        {
            return blob;
        }

        public void setBlob(byte[] blob)
        {
            this.blob = blob;
        }

        private Byte[] ToBlob(string blob_file_url)
        {
            FileStream fs;

            try
            {
                using (fs = new FileStream(blob_file_url, FileMode.Open, FileAccess.Read))
                {
                    using (BinaryReader br = new BinaryReader(fs))
                    {
                        byte[] blob = br.ReadBytes((int)fs.Length);

                        return blob;
                    }
                }
            }

            catch (FileNotFoundException blobException)
            {
                //med.CreateDialog("Fehler", "Fehler beim Schreiben der BLOB!");
            }

            return null;
        }

        public void CreateBlob()
        {
            this.blob = ToBlob(blob_s);
        }

        public override string ToString()
        {
            return "L:" + level + " C:" + col + " R:" + row;
        }
    }
}