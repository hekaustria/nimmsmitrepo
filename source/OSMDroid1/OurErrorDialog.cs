using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Lang;

namespace OSMDroid1
{
    class OurErrorDialog
    {
        public void CreateDialog(string title, string message, Context context) 
        {
            var builder = new AlertDialog.Builder(context);
            builder.SetTitle(title);
            builder.SetMessage(message);
            

            builder.SetPositiveButton("Ok", (EventHandler<DialogClickEventArgs>)null);
            var dialog = builder.Create();

            dialog.Show();
            var yesBtn = dialog.GetButton((int)DialogButtonType.Positive);

            yesBtn.Click += (sender, args) =>
            {
                dialog.Dismiss();
            };

        }
    }

}