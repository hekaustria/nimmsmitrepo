using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OSMDroid1.Model
{
    public abstract class BaseDocument : ICloneable
    {
        public const string ATTRIBUTE_TENANTID = "TENANTID";

        public const string ATTRIBUTE_ID = "ID";

        public string id
        {
            get;
            set;
        }

        public string TenantId
        {
            get;
            set;
        }

        public abstract string DocumentType
        {
            get;
        }

        public BaseDocument()
        {
            this.id = string.Empty;
            this.TenantId = string.Empty;
        }

        public virtual object Clone()
        {
            BaseDocument expr_0B = (BaseDocument)base.MemberwiseClone();
            expr_0B.id = string.Empty;
            expr_0B.TenantId = string.Empty;
            return expr_0B;
        }
    }
}