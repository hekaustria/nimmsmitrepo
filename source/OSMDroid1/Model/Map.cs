using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OSMDroid1.Model
{
    public class Map : BaseDocument
    {
        public const string DocumentTypeName = "Map";

        public const string ATTRIBUTE_NAME = "NAME";

        public const string ATTRIBUTE_DESCRIPTION = "DESCRIPTION";

        public const string ATTRIBUTE_CREATEDAT = "CREATEDAT";

        public const string ATTRIBUTE_ISPUBLIC = "ISPUBLIC";

        public const string ATTRIBUTE_VIEWERUITEMPLATE = "VIEWERUITEMPLATE";

        public override string DocumentType
        {
            get
            {
                return "Map";
            }
        }

        public string Name
        {
            get;
            set;
        }

        public string Description
        {
            get;
            set;
        }

        public DateTime CreatedAt
        {
            get;
            set;
        }

        public bool IsPublic
        {
            get;
            set;
        }

        public List<string> Users
        {
            get;
            set;
        }

        public string ViewerUiTemplate
        {
            get;
            set;
        }

        public Map()
        {
            this.Users = new List<string>();
        }
    }
}