using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.IO;
using Java.IO;
using Java.Util;
using Android.Content.Res;

namespace OSMDroid1
{
    class AndroidMapService : MapService
    {
        private Context context;

        private static readonly AndroidMapService instance = new AndroidMapService();

        public static AndroidMapService getInstance()
        {
            return instance;
        }

        public void setContext(Context context)
        {
            this.context = context;
        }

        //is always the first method that is called, therefore the context is passed here 
        public override string getUserName()
        {
            if (username == null)
            {
                username = context.GetSharedPreferences("pref_user", FileCreationMode.Private).GetString("username", "");
            }
            return username;
        }

        public override string getPw()
        {
            if (pw == null)
            {
                pw = context.GetSharedPreferences("pref_user", FileCreationMode.Private).GetString("pw", "");
            }
            return pw;
        }

        public override string getTenantId()
        {
            if (tenantId == null)
            {
                tenantId = context.GetSharedPreferences("pref_user", FileCreationMode.Private).GetString("tenantId", "");
            }
            return tenantId;
        }

        public override string getToken()
        {
            if (token == null)
            {
                token = context.GetSharedPreferences("pref_user", FileCreationMode.Private).GetString("token", "");
            }
            return token;
        }

        public override void storeUser()
        {
            //storing user data for updates 
            ISharedPreferences preferences = context.GetSharedPreferences("pref_user", FileCreationMode.Private);
            ISharedPreferencesEditor editor = preferences.Edit();
            editor.PutString("username", username);
            editor.PutString("pw", pw);
            editor.PutString("tenantId", tenantId);
            editor.PutString("token", token);
            editor.Commit();
        }

        public override void writeGeoCloudMapsToFile()
        {
            context.DeleteFile(FILENAME);
            Stream fos = context.OpenFileOutput(FILENAME, FileCreationMode.Private);
            
            Java.IO.PrintWriter writer = new PrintWriter(fos);

            foreach(var s in geoCloudMaps)
            {
                writer.Println(s);
                writer.Flush();
            }
            writer.Close();
        }

        //loads all GeoCloud maps from a file 
        public override void readGeoCloudMaps()
        {
            try
            {
                List<string> maps = new List<string>();
                Stream fis = context.OpenFileInput(FILENAME);
                Scanner scanner = new Scanner(fis);

                while (scanner.HasNextLine)
                {
                    maps.Add(scanner.NextLine());

                }
                this.geoCloudMaps = maps;
            }
            catch(Exception)
            {
                this.geoCloudMaps = new List<string>();
            }
        }


        protected override double calculateResolution(BruTile.Extent extent)
        {
            var metrics = context.Resources.DisplayMetrics;
            var width = metrics.WidthPixels;
            var height = metrics.HeightPixels;
            var dpi = metrics.Ydpi;
            return CalculateResolution(extent, width, height, dpi);
        }

        public override string getCapabilitiesFromXMLFile()
        {
            string capab = "";
            AssetManager assets = context.Assets;

            using (StreamReader sr = new StreamReader(assets.Open("WMTSCapabilities.xml")))
            {
                capab = sr.ReadToEnd();
            }

            return capab;
        }

        public override void logout()
        {
            ISharedPreferences preferences = context.GetSharedPreferences("pref_user", FileCreationMode.Private);
            ISharedPreferencesEditor editor = preferences.Edit();
            editor.PutString("username", null);
            editor.PutString("pw", null);
            editor.PutString("tenantId", null);
            editor.PutString("token", null);
            editor.Commit();

            context.DeleteFile(FILENAME);
        }
    }
}