using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Osmdroid.Views.Overlay;
using Osmdroid.Api;
using System.Collections;
using Osmdroid.Util;

//erstellt von Daniel Polzhofer

namespace OSMDroid1
{
    class OverlayEventHandler : Overlay, Osmdroid.Views.Overlay.ItemizedIconOverlay.IOnItemGestureListener
    {

        private Context ctx;
        private GeoPoint geoPoint;
        ArrayList anotherOverlayItemArray;
        ItemizedIconOverlay anotherItemizedIconOverlay;
        private float markerX;
        private float markerY;


        public OverlayEventHandler(Context ctx)
            : base(ctx)
        {
            this.ctx = ctx;
        }


        public override void Draw(Android.Graphics.Canvas c, Osmdroid.Views.MapView osmv, bool shadow)
        {

        }

        public override bool OnSingleTapUp(MotionEvent e, Osmdroid.Views.MapView mapView)
        {
            if (Map.overlay == true)
            {

                //if ((e.GetX() < (markerX - 70)) || (e.GetX() > (markerX + 70)))
                if ((markerX - 80) > e.GetX() || (markerX + 80) < e.GetX() || (markerY - 80) > e.GetY() || (markerY + 80) < e.GetY()) 
                {
                    Map.overlay = false;
                    mapView.OverlayManager.Remove(1);
                    mapView.Invalidate();
                }
               
            }

            return base.OnSingleTapUp(e, mapView);
        }

        public override bool OnLongPress(MotionEvent e, Osmdroid.Views.MapView mapView)
        {
            Vibrator vibrator = (Vibrator)ctx.GetSystemService(Context.VibratorService);
            vibrator.Vibrate(100);

            markerX = e.GetX();
            markerY = e.GetY();

            IProjection proj = mapView.Projection;
            var loc = proj.FromPixels((int)e.GetX(), (int)e.GetY());
            double longitude = loc.Longitude;
            double latitude = loc.Latitude;

            geoPoint = new GeoPoint(latitude, longitude);
            IMapController mapController = mapView.Controller;
           
            anotherOverlayItemArray = new ArrayList();
            anotherOverlayItemArray.Clear();
            
            anotherOverlayItemArray.Add(new OverlayItem("LongClickLocation", "", geoPoint));


            anotherItemizedIconOverlay = new ItemizedIconOverlay(ctx, anotherOverlayItemArray, this);

            if (Map.overlay != true)
            {
                Map.overlay = true;
            }
            else
            {
                mapView.OverlayManager.Remove(1);
            }
            mapView.OverlayManager.Add(1, anotherItemizedIconOverlay);
            mapView.Invalidate();



            return true;

        }



        public bool OnItemLongPress(int index, Java.Lang.Object item)
        {

            return false;
        }

        public bool OnItemSingleTapUp(int index, Java.Lang.Object item)
        {
         
            AlertDialog.Builder dialog = new AlertDialog.Builder(ctx);
            dialog.SetMessage("Wollen Sie hierher navigieren?");
            
            dialog.SetPositiveButton("Nein", (sender, e) =>
            {
                
            });
            dialog.SetNegativeButton("Ja", (sender, e) =>
            {
                //continue;
                Intent intent = new Intent(Android.Content.Intent.ActionView, Android.Net.Uri.Parse("http://maps.google.com/maps?&daddr=" + geoPoint.Latitude.ToString().Replace(",", ".") + "," + geoPoint.Longitude.ToString().Replace(",", ".")));
                ctx.StartActivity(intent);
            });
            dialog.Show();
           
            return true;
        }



        public void SetPoint(GeoPoint geoPoint,Osmdroid.Views.MapView mapView)
        {
            IMapController mapController = mapView.Controller;
            mapController.SetZoom(18);
            mapController.AnimateTo(geoPoint);
            ArrayList anotherOverlayItemArray = new ArrayList();
            anotherOverlayItemArray.Add(new OverlayItem("POI", "", geoPoint));

            anotherItemizedIconOverlay = new ItemizedIconOverlay(ctx, anotherOverlayItemArray, this);
            if (Map.overlay != true)
            {
                Map.overlay = true;

            }
            else
            {
                mapView.OverlayManager.Remove(1);
            }
            mapView.OverlayManager.Add(1, anotherItemizedIconOverlay);
            mapView.Invalidate();
        }


  

      
        

      
    }
}