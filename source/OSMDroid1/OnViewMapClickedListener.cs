using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace OSMDroid1
{
    class OnViewMapClickedListener : Java.Lang.Object, View.IOnClickListener
    {
        private string mapname;
        private Context context;

        public OnViewMapClickedListener(string mapname, Context context)
        {
            this.mapname = mapname;
            this.context = context;
        }
        public void OnClick(View v)
        {
            Intent mbtiles = new Intent(context, typeof(Map));
            mbtiles.PutExtra("Filename", mapname);
            context.StartActivity(mbtiles);
        }
    }
}