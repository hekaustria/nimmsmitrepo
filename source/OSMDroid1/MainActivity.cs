﻿using Android.App;
using Android.Content;
using Android.Views;
using Android.OS;
using System;
using Android.Util;
using System.Collections.Generic;
using System.Linq;
using Mono.Data.Sqlite;
using System.IO;

/*
 * 
 * Erstellt von Daniel Polzhofer
 * */

namespace OSMDroid1
{
    [Activity(Label = "PSS Maps", MainLauncher = true, Icon = "@drawable/icon")]

    public class MainActivity : Activity, IDownloadListener, ActionBar.ITabListener
    {
        private MapFragment mapFragment = new MapFragment();
        private DownloadFragment downloadFragment = new DownloadFragment();
        private AndroidMapService service = AndroidMapService.getInstance();
        private Database db;


        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            service.setContext(this);
            if (service.getUserName().Equals(""))
            {
                Intent intent = new Intent(this, typeof(LoginActivity));
                StartActivity(intent);
            }
            else
            {

                service.readGeoCloudMaps();
                //db = new Database("database");

            }


            this.ActionBar.NavigationMode = ActionBarNavigationMode.Tabs;
            var tabMaps = this.ActionBar.NewTab();
            tabMaps.SetText("Maps");
            var tabDownload = this.ActionBar.NewTab();
            tabDownload.SetText("Download");

            ISharedPreferences preferences = GetSharedPreferences("pref_user", FileCreationMode.Private);
            string s = preferences.GetString("username", "");


            tabMaps.TabSelected += delegate (object sender, ActionBar.TabEventArgs e)
            {
                e.FragmentTransaction.Replace(Resource.Id.fragments, mapFragment, "mapfragment");
                
            };

            tabDownload.TabSelected += delegate (object sender, ActionBar.TabEventArgs e)
            {
                e.FragmentTransaction.Replace(Resource.Id.fragments, downloadFragment, "downloadfragment");
            };


           // IMenuItem menutenantId = (IMenuItem)FindViewById(Resource.Id.action_tenantid);

            
            this.ActionBar.AddTab(tabMaps);
            this.ActionBar.AddTab(tabDownload);

            //tabMaps.SetTabListener(this);
            //tabDownload.SetTabListener(this);

            service.setDownloadListener(mapFragment);
            service.setDownloadListener(downloadFragment);
            //service.setDownloadListener(this);

            SetContentView(Resource.Layout.Main);

        }


        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater inflater = this.MenuInflater;
            inflater.Inflate(Resource.Menu.main_menu, menu);


            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            Intent intent;
            switch (item.ItemId)
            {
                case Resource.Id.action_showuser:
                    CurrentUserDialog dialog = new CurrentUserDialog();
                    dialog.CreateDialog(this, service.getTenantId(),service.getUserName());
                    break;
                /*case Resource.Id.action_logout:
                    
                    service.logout();
                    intent = new Intent(this, typeof(LoginActivity));
                    StartActivity(intent);
                    this.Finish();
                    break;*/
                case Resource.Id.action_sync:
                    service.loadMapsInGeoCloud();
                    break;
            }

            return true;

        }
        public override void OnBackPressed()
        {
            base.OnBackPressed();
        }

        public MapFragment getMapFragment()
        {
            return mapFragment;
        }

        public DownloadFragment getDownloadFragment()
        {
            return downloadFragment;
        }

        protected override void OnStop()
        {
            base.OnStop();
        }

        protected override void OnDestroy()
        {
            base.OnDestroy();

            //if app is closed during downloading a map
            foreach(ProgressFile f in service.getDownloadedMapFiles())
            {
                if(f.getProgress() != 100)
                {
                    f.Delete();
                }
            }

        }

        public void downloadStarted()
        {
            this.ActionBar.SetSelectedNavigationItem(0);
            this.ActionBar.SelectTab(this.ActionBar.GetTabAt(0));
            
        }

        public void progressChanged()
        {
            
        }

        public void downloadComplete()
        {
            
        }

        public void downloadButtonClicked()
        {
        }

        public void OnTabReselected(ActionBar.Tab tab, FragmentTransaction ft)
        {
        }

        public void OnTabSelected(ActionBar.Tab tab, FragmentTransaction ft)
        {
            switch (tab.Position)
            {
                case 0:
                    ft.Replace(Resource.Id.fragments, mapFragment, "mapfragment");
                    break;

                case 1:
                    ft.Replace(Resource.Id.fragments, downloadFragment, "downloadfragment");
                    break;

                default:
                    break;
            }
        }

        public void OnTabUnselected(ActionBar.Tab tab, FragmentTransaction ft)
        {
        }

        
    }
}