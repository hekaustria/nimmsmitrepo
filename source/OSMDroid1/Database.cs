using Mono.Data.Sqlite;
using System.Collections.Generic;
using SQLite;
using System.Threading.Tasks;
using System;

namespace OSMDroid1
{
    class Database
    {
        //private static SqliteConnection database_connection; //Datenbankverbindung
        private static string database_name; //Standard Speicherort der Datenbank
        private static string database_url;

        //Definition der Tabellen
        private static string createTiles_text = "CREATE TABLE tiles (zoom_level integer,tile_column integer,tile_row integer,tile_data blob)";
        private static string createMetadata_text = "CREATE TABLE metadata (name text, value text)";
        private static string createLocations_text = "CREATE TABLE locations (ID INTEGER NOT NULL, cordx NUMERIC NOT NULL,cordy NUMERIC NOT NULL,name TEXT NOT NULL, PRIMARY KEY(ID))";

        //Definition f�r Inserts
        private static string insertMetadata_text = "INSERT INTO metadata VALUES(@name, @value)";
        private static string insertTiles_text = "INSERT INTO tiles VALUES(@level, @col, @row, @blob)";
        private static string insertLocations_text = "INSERT INTO locations VALUES(@id, @x, @y, @name)";

        public static bool cancel = false;


        public Database(string databaseName)
        {
            database_name = "Data Source=" + databaseName + ";Version=3;";
            SqliteConnection.CreateFile(databaseName);
            //CreateConnection(databaseName);
            InitTables();


        }

        public Database(string databaseName, List<Metadata> metadata_list, List<Tile> tile_list)
        {
            database_name = "Data Source=" + databaseName + ";Version=3;";
            SqliteConnection.CreateFile(databaseName);
            //CreateConnection(databaseName); //Verbindung zu Datenbank aufbauen
            InitTables(); //Tabellen und Indizes initalisieren
            AddMetadataToDatabase(metadata_list); //Metadaten hinzuf�gen
            AddTilesToDatabase(tile_list); //Tiles hinzuf�gen
        }

        /*
        private void CreateConnection(string databaseName)
        {
            
            using (database_connection = new SqliteConnection("Data Source=" + databaseName + ";Version=3;"))
            {

                try
                {
                    var transaction = database_connection.BeginTransaction();
                    
                    database_connection.Open();
                    
                }

                catch (SqliteException e)
                {
                    throw new DatabaseException();
                }
            }
        }
        */

        //Beendet die Verbindung zur Datenbank bzw. dem File
        /*
        private void EndConnection()
        {
            try
            {
                database_connection.Close();
            }

            catch (SqliteException e)
            {
                throw new DatabaseException();
            }
        }
        

        private bool IsOpen()
        {
            if (database_connection.State == System.Data.ConnectionState.Open)
            {
                return true;
            }
            return false;
        }
        */

        private void InitTables()
        {

            try
            {
                using (var conn = new SqliteConnection(database_name))
                {
                    
                    
                    conn.OpenAsync();

                    using (SqliteCommand createTiles = new SqliteCommand(conn), 
                        createMetadata = new SqliteCommand(conn),
                            createLocations = new SqliteCommand(conn))
                    {
                        createTiles.CommandText = createTiles_text;
                        createMetadata.CommandText = createMetadata_text;
                        createLocations.CommandText = createLocations_text;
                        createMetadata.ExecuteNonQueryAsync();
                        createTiles.ExecuteNonQueryAsync();
                        createLocations.ExecuteNonQueryAsync();
                    }
                    
                }
            }
            catch (SqliteException e)
            {

                throw new DatabaseException();
            }


        }

        //Tr�gt die Metadaten ein
        public void AddMetadataToDatabase(List<Metadata> metadata_list)
        {

            using (var conn = new SqliteConnection(database_name))
            {
                conn.OpenAsync();
                foreach (var metadata in metadata_list)
                {
                    if (Database.cancel != true)
                    {
                        using (SqliteCommand insertMetadata = conn.CreateCommand(), 
                            selectCommand = conn.CreateCommand())
                        {
                            insertMetadata.CommandText = insertMetadata_text;

                            insertMetadata.Parameters.Add("@name", System.Data.DbType.String).Value = metadata.getName();
                            insertMetadata.Parameters.Add("@value", System.Data.DbType.String).Value = metadata.getValue();

                            selectCommand.CommandText = "Select value From metadata";

                            try
                            {
                                insertMetadata.ExecuteNonQueryAsync();
                                /*
                                var readerTask = selectCommand.ExecuteReaderAsync();
                                var reader = readerTask.Result;
                                while (reader.Read())
                                {
                                    string value = reader["value"].ToString();
                                }
                                reader.Close();
                                */
                            }

                            catch (SqliteException e)
                            {
                                throw new DatabaseException();
                            }
                        }
                    }
                }
            }


        }

        //Tr�gt die Tile-Daten ein
        public void AddTilesToDatabase(List<Tile> tile_list)
        {

            //Beispiel zum Einf�gen von Blobdateien http://www.akadia.com/services/dotnet_read_write_blob.html
            using (var conn = new SqliteConnection(database_name))
            {
                conn.OpenAsync();
                foreach (var tile in tile_list)
                {
                    if (Database.cancel != true)
                    {
                        using (SqliteCommand insertTile = conn.CreateCommand())
                        {
                            tile.CreateBlob(); //blob wird erstellt
                            insertTile.CommandText = insertTiles_text;

                            insertTile.Parameters.Add("@level", System.Data.DbType.Int32).Value = tile.GetLevel();
                            insertTile.Parameters.Add("@col", System.Data.DbType.Int32).Value = tile.GetCol();
                            insertTile.Parameters.Add("@row", System.Data.DbType.Int32).Value = tile.GetRow();
                            insertTile.Parameters.Add("@blob", System.Data.DbType.Object, tile.GetBlob().Length).Value = tile.GetBlob();

                            try
                            {
                                insertTile.ExecuteNonQueryAsync();
                            }

                            catch (SqliteException e)
                            {
                                throw new DatabaseException();
                            }
                        }

                    }
                }
            }
            

        }

        //Tr�gt die Location-Daten ein
        public void AddLocationsToDatabase(List<Location> location_list)
        {
            using (var conn = new SqliteConnection(database_name))
            {
                foreach (var location in location_list)
                {
                    using (SqliteCommand insertLocation = conn.CreateCommand()) 
                    {
                        insertLocation.CommandText = insertLocations_text;

                        insertLocation.Parameters.Add("@id", System.Data.DbType.Int32).Value = location.getID();
                        insertLocation.Parameters.Add("@x", System.Data.DbType.Double).Value = location.getCord_x();
                        insertLocation.Parameters.Add("@y", System.Data.DbType.Double).Value = location.getCord_y();
                        insertLocation.Parameters.Add("@name", System.Data.DbType.String).Value = location.getName();

                        try
                        {
                            insertLocation.ExecuteNonQueryAsync();
                        }
                        catch (SqliteException e)
                        {
                            throw new DatabaseException();
                        }
                    }
                }
            }

        }
    }
}