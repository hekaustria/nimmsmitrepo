using System;

using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using System.Collections.Generic;

/*
   Erstellt von Daniel Polzhofer
 */
namespace OSMDroid1.FileExplorer
{

    public class FileAdapter : ArrayAdapter<FileItem>
    {
        private Context c;
        private int id;
        private List<FileItem> fItems;



        public FileAdapter(Context context, int textViewResourceId, List<FileItem> files)
            : base(context, textViewResourceId, files)
        {
            c = context;
            id = textViewResourceId;
            fItems = files;
        }

        public FileItem GetFileItem(int i)
        {
            return fItems[i];
        }



        public override View GetView(int position, View convertView, ViewGroup parent)
        {
            View v = convertView;

            if (v == null)
            {
                LayoutInflater li = (LayoutInflater)c.GetSystemService(Context.LayoutInflaterService);
                v = li.Inflate(id, null);
            }


            FileItem o = fItems[position];
            if (o != null)
            {
                TextView t1 = (TextView)v.FindViewById(Resource.Id.file_picker_text);
                TextView t2 = (TextView)v.FindViewById(Resource.Id.size_id);
                TextView t3 = (TextView)v.FindViewById(Resource.Id.date_id);
                if (o.IsFile())
                {
                    ImageView imageView = (ImageView)v.FindViewById(Resource.Id.file_picker_image);
                    var image = c.Resources.GetDrawable(Resource.Drawable.mbtiles);
                    imageView.SetImageDrawable(image);

                    t2.SetText(o.GetSize().Replace(",","."), TextView.BufferType.Normal);
                    
                    
                    t2.SetTextColor(Android.Graphics.Color.Gray);

                }
                else
                {
                    ImageView imageView = (ImageView)v.FindViewById(Resource.Id.file_picker_image);
                    var image = c.Resources.GetDrawable(Resource.Drawable.normal_folder);
                    imageView.SetImageDrawable(image);


                }
                t1.SetText(o.GetName(), TextView.BufferType.Normal);
                t3.SetText(o.GetDate().ToString(), TextView.BufferType.Normal);
                t3.SetTextColor(Android.Graphics.Color.Gray);


            }
            return v;
        }


    }
}