﻿using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using Java.IO;
using System.Threading;
using System.Threading.Tasks;

/*
   Erstellt von Daniel Polzhofer
 */
namespace OSMDroid1.FileExplorer
{
    [Activity(Label = "PSS Maps",ConfigurationChanges=Android.Content.PM.ConfigChanges.Orientation | Android.Content.PM.ConfigChanges.ScreenSize)]

    public class FileExplorer : ListActivity, IDialogInterfaceOnKeyListener
    {

        private Java.IO.File currentDir;
        private FileAdapter adapter;
        private Java.IO.File ziel;
        private bool copybreak;
        private Thread t;
        private ProgressDialog pd;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
            currentDir = new Java.IO.File("/");
            ShowFiles(currentDir);
        }

        private void ShowFiles(Java.IO.File f)
        {
            Java.IO.File[] dirs = f.ListFiles();
            List<FileItem> dir = new List<FileItem>();
            List<FileItem> files = new List<FileItem>();
           

            foreach (Java.IO.File f2 in dirs)
            {
                if (f2.IsDirectory && currentDir.Path.Equals("/") == false)
                {
                    dir.Add(new FileItem(f2.Name, false, f2.AbsolutePath, f2.LastModified()));
                }
                else
                    if (f2.IsDirectory && currentDir.Path.Equals("/") == true)
                    {
                        if (f2.Name.Contains("sdcard"))
                        {
                            files.Add(new FileItem(f2.Name, false, f2.AbsolutePath, f2.LastModified()));
                        }
                    }
                    else
                        if (f2.Name.EndsWith(".mbtiles"))
                        {
                            files.Add(new FileItem(f2.Name, true, f2.AbsolutePath, f2.Length(), f2.LastModified()));
                        }
            }


            dir.AddRange(files);

            adapter = new FileAdapter(this, Resource.Layout.Main_Explorer, dir);
            this.ListAdapter = adapter;

        }


        protected override void OnListItemClick(ListView l, View v, int position, long id)
        {
            base.OnListItemClick(l, v, position, id);
            FileItem fItem = adapter.GetFileItem(position);
            if (fItem.IsFile())
            {
                Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this);
                Android.App.AlertDialog alertDialog = builder.Create();
                alertDialog.SetTitle("Import Karte");
                alertDialog.SetMessage("Wollen Sie diese Karte wirklich importieren? ");
                alertDialog.SetButton("Nein", (sender, e) =>
                {

                });
                alertDialog.SetButton2("Ja", (sender, e) =>
                {

                    String quellePath = fItem.GetPath().Remove((fItem.GetPath().Length - fItem.GetName().Length), fItem.GetName().Length);
                    Java.IO.File quelle = new Java.IO.File(quellePath, fItem.GetName());
                    String dir = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal);
                    ziel = new Java.IO.File(dir + "/mbtiles", fItem.GetName());
                    AndroidMapService service = AndroidMapService.getInstance();
                    try
                    {
                        if (ziel.Exists())
                        {
                            Android.App.AlertDialog.Builder builder2 = new Android.App.AlertDialog.Builder(this);
                            Android.App.AlertDialog alertDialog2 = builder2.Create();
                            alertDialog2.SetTitle("File ist schon vorhanden");
                            alertDialog2.SetMessage("Wollen Sie das vorhandene File ersetzen?");
                            alertDialog2.SetButton("Nein", (sender2, e2) =>
                            {

                            });
                            alertDialog2.SetButton2("Ja", (sender2, e2) =>
                            {
                                ziel.Delete();
                                CopyThread(quelle, ziel);
                                service.addDownloadedMap(ProgressFile.toProgressFile(ziel));
                               
                            });
                            alertDialog2.Show();

                        }
                        else
                        {
                            CopyThread(quelle, ziel);
                            service.addDownloadedMap(ProgressFile.toProgressFile(ziel));
                        }



                    }
                    catch (Exception ex)
                    {
                        System.Console.WriteLine(ex);
                    }
                    

                });
                alertDialog.Show();
            }
            else
            {

                currentDir = new Java.IO.File(fItem.GetPath());
                ShowFiles(currentDir);
            }

            

        }

        
      
        public override void OnBackPressed()
        {
                String path = currentDir.Path;
                int length = path.Length;
                int l = currentDir.Name.Length;


                String newParth = path.Remove((length - l), l);
                currentDir = new Java.IO.File(newParth);
                ShowFiles(currentDir);

                if (currentDir.Path.Equals("/"))
                {
                    if (l == 0)
                    {
                        Intent intent = new Intent(this, typeof(MainActivity));
                        StartActivity(intent);
                        this.Finish();
                    }

                }      

        }


        public void CopyThread(Java.IO.File quelle, Java.IO.File ziel)
        {
            pd = new ProgressDialog(this);
            pd.SetMessage("Karte wird importiert");
            pd.SetProgressStyle(ProgressDialogStyle.Spinner);
            pd.Indeterminate = true;
            pd.SetCanceledOnTouchOutside(false);
            pd.SetOnKeyListener(this);
            pd.Show();



            t = new Thread(() =>
            {
                copybreak = false;
                ziel.CreateNewFile();
                FileCopy(quelle, ziel);
               
               
            });

            t.Start();
        }

        public void FileCopy(Java.IO.File src, Java.IO.File dst) 
        {
            FileInputStream inputStream= new FileInputStream(src);
            FileOutputStream outputStream = new FileOutputStream(dst);

            // Transfer bytes from in to out
            byte[] buf = new byte[2048];
            int len;

            try
            {
                while ((len = inputStream.Read(buf)) > 0 && copybreak != true)
                {
                    outputStream.Write(buf, 0, len);
                }
                pd.Hide();
            }catch (Exception ex)
                    {
                        System.Console.WriteLine(ex);
                        
                    }

            System.Console.WriteLine("Wurde beendet");
            inputStream.Close();
            outputStream.Close();
          
            if(copybreak == false)
            {
                MetadataDAO.initMetadata(ziel.AbsolutePath);
                
                Intent intent = new Intent(this, typeof(MainActivity));
                StartActivity(intent);
                
                this.Finish();
             
            }
            else
            {
                ziel.Delete();
            }
   
            
         }

      

        public bool OnKey(IDialogInterface dialog, Keycode keyCode, KeyEvent e)
        {
            if (keyCode == Keycode.Back)
            {
                pd.Hide();
                copybreak = true;
                     
                return true;
            }
            return false;
        }
    }

}

