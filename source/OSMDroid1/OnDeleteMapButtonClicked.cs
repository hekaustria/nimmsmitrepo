using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Java.Util;

namespace OSMDroid1
{
    class OnDeleteMapButtonClicked : Java.Lang.Object, View.IOnClickListener
    {
        private int position;
        private Context context;

        public OnDeleteMapButtonClicked(int position, Context context)
        {
            this.position = position;
            this.context = context;
        }

        public void OnClick(View v)
        {
            AndroidMapService service = AndroidMapService.getInstance();
            List<ProgressFile> files = service.getDownloadedMapFiles();
            if (files != null)
            {
                Android.App.AlertDialog.Builder builder = new Android.App.AlertDialog.Builder(this.context);
                Android.App.AlertDialog alertDialog = builder.Create();
                alertDialog.SetTitle("Delete map");
                alertDialog.SetMessage("Do you really want to delete the map?");
                alertDialog.SetButton("No", (sender, e) =>
                {

                });

                alertDialog.SetButton2("Yes", (sender, e) =>
                {
                    var file = files[position];
                    files.Remove(file);
                    file.Delete();
                    MainActivity activity = (MainActivity)context;
                    activity.getMapFragment().ShowFiles(); 
                    
                });
                alertDialog.Show();
            }
        }
    }
}